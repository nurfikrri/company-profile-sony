const path = require('path');
const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const app = express();
const busboy = require("then-busboy");
const fileUpload = require('express-fileupload');
const mv = require('mv');

const connection=mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'admin',
    database:'sonyprofile'
});

connection.connect(function(error){
    if(!!error) console.log(error);
    else console.log('Database Connected!');
}); 

//set views file
app.set('views',path.join(__dirname,'views'));
			
//set view engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/public/images/', express.static('./public/images'));
app.use(fileUpload());



app.get('/',(req, res) => {
    // res.send('CRUD Operation using NodeJS / ExpressJS / MySQL');
    let sql = "SELECT * FROM tentang_kami";
    let query = connection.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('tentang_index', {
            title : 'Profil Perusahaan',
            tentang_kami : rows
        });
    });
});

app.get('/home',(req, res) => {
    // res.send('CRUD Operation using NodeJS / ExpressJS / MySQL');
    let sql = "SELECT * FROM tentang_kami where id";
    let query = connection.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('home', {
            title : 'Profil Perusahaan',
            tentang_kami : rows
        });
    });
});

app.get('/tim',(req, res) => {
    // res.send('CRUD Operation using NodeJS / ExpressJS / MySQL');
    let sql = "SELECT * FROM tim";
    let query = connection.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('tim_index', {
            title : 'Profil anggota',
            tim : rows
        });
    });
});

app.get('/tim_home',(req, res) => {
    // res.send('CRUD Operation using NodeJS / ExpressJS / MySQL');
    let sql = "SELECT * FROM tim";
    let query = connection.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('tim_home', {
            title : 'Profil anggota',
            tim : rows
        });
    });
});

app.get('/produk',(req, res) => {
    // res.send('CRUD Operation using NodeJS / ExpressJS / MySQL');
    let sql = "SELECT * FROM produk";
    let query = connection.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('produk_index', {
            title : 'Produk sony',
            produk : rows
        });
    });
});

app.get('/produk_home',(req, res) => {
    // res.send('CRUD Operation using NodeJS / ExpressJS / MySQL');
    let sql = "SELECT * FROM produk";
    let query = connection.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('produk_home', {
            title : 'Produk sony',
            produk : rows
        });
    });
});

app.get('/kontak',(req, res) => {
    // res.send('CRUD Operation using NodeJS / ExpressJS / MySQL');
    let sql = "SELECT * FROM kontak";
    let query = connection.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('kontak_index', {
            title : 'Inbox Saran',
            kontak : rows
        });
    });
});


app.get('/add',(req, res) => {
    res.render('user_add', {
        title : 'CRUD Operation using NodeJS / ExpressJS / MySQL'
    });
});



app.get('/addproduk',(req, res) => {
    res.render('produk_add', {
        title : 'Tambah Produk'
    });
});

app.get('/addtim',(req, res) => {
    res.render('tim_add', {
        title : 'Tambah Anggota TIM'
    });
});





app.post('/savekontak',(req, res) => { 
    let data = {nama: req.body.name, email: req.body.email, saran: req.body.comments};
    let sql = "INSERT INTO kontak SET ?";
    let query = connection.query(sql, data,(err, results) => {
      if(err) throw err;
      res.redirect('/home');
    });
});

app.post('/savekontak_produk',(req, res) => { 
    let data = {nama: req.body.name, email: req.body.email, saran: req.body.comments};
    let sql = "INSERT INTO kontak SET ?";
    let query = connection.query(sql, data,(err, results) => {
      if(err) throw err;
      res.redirect('/produk_home');
    });
});

app.post('/savekontak_tim',(req, res) => { 
    let data = {nama: req.body.name, email: req.body.email, saran: req.body.comments};
    let sql = "INSERT INTO kontak SET ?";
    let query = connection.query(sql, data,(err, results) => {
      if(err) throw err;
      res.redirect('/tim_home');
    });
});

app.post('/saveproduk',(req, res) => { 
    var file = req.files.foto;
		var img_name=file.name;
            file.mv('public/images/upload_image/produk/'+file.name, function(err) {
                
                             
                    if (err)
   
                      return res.status(500).send(err);
    
    let data = {namaproduk: req.body.namaproduk, harga: req.body.harga, spesifikasi: req.body.spesifikasi, foto: img_name};
    let sql = "INSERT INTO produk SET ?";
    let query = connection.query(sql, data,(err, results) => {
      if(err) throw err;
      res.redirect('/produk');
    });
});
})



app.post('/savetim',(req, res) => { 
    var file = req.files.foto;
		var img_name=file.name;
            file.mv('public/images/upload_image/tim/'+file.name, function(err) {
                
                             
                    if (err)
   
                      return res.status(500).send(err);
    
    let data = {namatim: req.body.namatim, NIM: req.body.NIM, jobdesk: req.body.jobdesk, foto :img_name};
    let sql = "INSERT INTO Tim SET ?";
    let query = connection.query(sql, data,(err, results) => {
      if(err) throw err;
      res.redirect('/tim');
    });
});
})

app.get('/edit/:userId',(req, res) => {
    const userId = req.params.userId;
    let sql = `Select * from users where id = ${userId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.render('user_edit', {
            title : 'CRUD Operation using NodeJS / ExpressJS / MySQL',
            user : result[0]
        });
    });
});

app.get('/edittentang/:tentangId',(req, res) => {
    const tentangId = req.params.tentangId;
    let sql = `Select * from tentang_kami where id = ${tentangId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.render('tentang_edit', {
            title : 'Edit Keterangan Perusahaan',
            tentang_kami : result[0]
        });
    });
});

app.get('/editproduk/:produkId',(req, res) => {
    const produkId = req.params.produkId;
    let sql = `Select * from produk where id = ${produkId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.render('produk_edit', {
            title : 'Edit Produk',
            produk : result[0]
        });
    });
});

app.get('/edittim/:timId',(req, res) => {
    const timId = req.params.timId;
    let sql = `Select * from tim where id = ${timId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.render('tim_edit', {
            title : 'Edit Anggota',
            tim : result[0]
        });
    });
});


app.post('/update',(req, res) => {
    const userId = req.body.id;
    let sql = "update users SET name='"+req.body.name+"',  email='"+req.body.email+"',  phone_no='"+req.body.phone_no+"' where id ="+userId;
    let query = connection.query(sql,(err, results) => {
      if(err) throw err;
      res.redirect('/');
    });
});

app.post('/updatetentang',(req, res) => {
    const tentangId = req.body.id;
    
    let sql = "update tentang_kami SET sejarah='"+req.body.sejarah+"',  visi='"+req.body.visi+"',  misi='"+req.body.misi+"' where id ="+tentangId;
    let query = connection.query(sql,(err, results) => {
      if(err) throw err;
      res.redirect('/');
    });
});


app.post('/updateproduk',(req, res) => {
    const produkId = req.body.id;
   
    let sql = "update produk SET namaproduk='"+req.body.namaproduk+"',  harga='"+req.body.harga+"',  spesifikasi='"+req.body.spesifikasi+"'  where id ="+produkId;
    let query = connection.query(sql,(err, results) => {
      if(err) throw err;
      res.redirect('/produk');
    });
});


app.post('/updatetim',(req, res) => {
    const timId = req.body.id;
    
    let sql = "update tim SET namatim='"+req.body.namatim+"',  NIM='"+req.body.NIM+"',  jobdesk='"+req.body.jobdesk+"' where id ="+timId;
    let query = connection.query(sql,(err, results) => {
      if(err) throw err;
      res.redirect('/tim');
    });
});



app.get('/delete/:userId',(req, res) => {
    const userId = req.params.userId;
    let sql = `DELETE from users where id = ${userId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.redirect('/');
    });
});

app.get('/deletetentang/:tentangId',(req, res) => {
    const tentangId = req.params.tentangId;
    let sql = `DELETE from tentang_kami where id = ${tentangId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.redirect('/');
    });
});

app.get('/deleteproduk/:produkId',(req, res) => {
    const produkId = req.params.produkId;
    let sql = `DELETE from produk where id = ${produkId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.redirect('/produk');
    });
});

app.get('/deletetim/:timId',(req, res) => {
    const timId = req.params.timId;
    let sql = `DELETE from tim where id = ${timId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.redirect('/tim');
    });
});

app.get('/deletekontak/:kontakId',(req, res) => {
    const kontakId = req.params.kontakId;
    let sql = `DELETE from kontak where id = ${kontakId}`;
    let query = connection.query(sql,(err, result) => {
        if(err) throw err;
        res.redirect('/kontak');
    });
});


// Server Listening
app.listen(3000, () => {
    console.log('Server is running at port 3000');
});